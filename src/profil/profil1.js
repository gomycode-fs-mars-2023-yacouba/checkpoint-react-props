import React from 'react';
import PropTypes from 'prop-types';

const Profile = ({ fullName, bio, profession, handleName, children, style }) => {
  return (
    <div style={{ backgroundColor: '#f9f9f9', padding: '20px', ...style }}>
      {children}
      <h1>{fullName}</h1>
      <h2>{profession}</h2>
      <p>{bio}</p>
      <button onClick={() => handleName(fullName)}>Alert Name</button>
    </div>
  );
};

Profile.propTypes = {
  fullName: PropTypes.string.isRequired,
  bio: PropTypes.string.isRequired,
  profession: PropTypes.string.isRequired,
  handleName: PropTypes.func.isRequired,
  children: PropTypes.element.isRequired,
  style: PropTypes.object,
};

Profile.defaultProps = {
  fullName: 'John Doe',
  bio: '',
  profession: '',
  handleName: () => {},
  style: {},
};

export default Profile;
