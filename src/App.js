import React from 'react';
import Profile from './profil/profil1';
import image from './profil/profi-pic.jfif';

function App() {
  const handleName = (name) => alert(`Hello ${name}`);

  return (
    <div className="App">
      <Profile
        fullName="John Smith"
        bio="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet leo dui."
        profession="Web Developer"
        handleName={handleName}
        style={{ margin: '20px', border: '2px solid #ccc' }}
      >
        <img src={image} alt="Profile" />
      </Profile>
    </div>
  );
}

export default App;
